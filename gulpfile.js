var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

//elixir(function(mix) {cd cd
//    mix.sass('app.scss');
//});

elixir(function (mix) {
    var jsDir = 'resources/assets/js/',
        bowerDir = 'bower_components/';
    mix.copy(bowerDir + 'jquery/dist/jquery.min.js', jsDir);
    mix.copy(bowerDir + 'moment/min/moment.min.js', jsDir);
    mix.copy(bowerDir + 'bootstrap/dist/js/bootstrap.min.js', jsDir);
    mix.copy(bowerDir + 'eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js', jsDir);
    mix.copy(bowerDir + 'oh-snap/ohsnap.min.js', jsDir);
    mix.copy(bowerDir + 'mustache.js/mustache.min.js', jsDir);
    mix.scripts([
        'jquery.min.js',
        'moment.min.js',
        'bootstrap.min.js',
        'bootstrap-datetimepicker.min.js',
        'mustache.min.js',
        'ohsnap.min.js'
    ], 'public/js/vendor.js');
});