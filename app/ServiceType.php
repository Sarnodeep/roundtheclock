<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceType extends Model
{
    use SoftDeletes;
    protected $fillable = ['service_type_name', 'description'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
}
