<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceBooking extends Model
{
    use SoftDeletes;

    protected $fillable = ['name', 'mobile', 'booking_date', 'booking_time', 'service_type_id', 'location_id', 'status'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    public function serviceType()
    {
        return $this->belongsTo('App\ServiceType');
    }
    public function location()
    {
        return $this->belongsTo('App\Location');
    }

}
