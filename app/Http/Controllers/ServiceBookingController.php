<?php

namespace App\Http\Controllers;


use App\Http\Requests\ServiceBookingRequest;
use App\Location;
use App\ServiceBooking;
use App\ServiceType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


/**
 * Class ServiceBookingController
 * @package App\Http\Controllers
 */
class ServiceBookingController extends Controller
{
    /**
     * max no of booking per day
     * @var int
     */
    protected $limit = 10;


    public function getNo($i)
    {
        $no = rand(1, 10);
        if (in_array($no, $i)) {
            return $this->getNo($i);
        }
        return $no;
    }

    /**
     * returns the Index web page
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $serviceTypes = ServiceType::get(['service_type_name', 'id']);
        $locations = Location::get();
        return view('web.index', compact('serviceTypes', 'locations'));
    }

    /**
     * returns the sevices page
     * @return \Illuminate\View\View
     */
    public function service()
    {
        $serviceTypes = ServiceType::get();
        return view('web.service', compact('serviceTypes'));
    }

    /**
     * returns the contact page
     * @return \Illuminate\View\View
     */
    public function contact()
    {
        return view('web.contact');
    }

    /**
     * return the about page
     * @return \Illuminate\View\View
     */
    public function about()
    {
        return view('web.about');
    }

    /**
     * return the privacy page
     * @return \Illuminate\View\View
     */
    public function privacy()
    {
        return view('web.privacy');
    }

    /**
     * this returns all the service type as API
     * @return array
     */
    public function getServiceType()
    {
        return ['data' => array_map(function ($data) {
            return [
                'service_type_id' => $data['id'],
                'name' => $data['service_type_name'],
                'description' => $data['description']
            ];
        }, ServiceType::get()->toArray())];
    }

    /**
     * this function place a booking
     *
     * @param ServiceBookingRequest $request
     * @return array|static
     */
    public function bookService(ServiceBookingRequest $request)
    {

        /**
         * @var  $serviceBooking
         *  a new instance of booking
         */
        $serviceBooking = new ServiceBooking();

        /**
         * if any slot available
         * place the booking
         */
        if ($this->availableBooking($serviceBooking, $request)) {
            $result = $serviceBooking::create($request->input());
            return ($request->has('_url')) ? view('web.thankyou') : $result;
        }

        return ['data' => 'Booking not Possible'];
    }

    /**
     * if slot available or not
     *
     * @param Model $model
     * @param Request $request
     * @return bool
     */
    public function availableBooking(Model $model, Request $request)
    {
        return true;
        return ($model->where('booking_date', $request->booking_date)
                ->where('booking_time', $request->booking_time)
                ->get()
                ->count() < $this->limit) ? true : false;
    }

}