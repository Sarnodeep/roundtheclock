<?php

namespace App\Http\Controllers\Admin;

use App\ServiceType;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

/**
 * Class ServiceTypeController
 * @package App\Http\Controllers\Admin
 */
class ServiceTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $serviceTypes = ServiceType::get();
        return view('admin.serviceType.index', compact('serviceTypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Requests\ServiceTypeRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\ServiceTypeRequest $request)
    {
        ServiceType::create($request->input());
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $serviceType = ServiceType::findOrFail($id);
        return view('admin.serviceType.edit', compact('serviceType'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Requests\ServiceTypeRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\ServiceTypeRequest $request, $id)
    {
        $serviceType = ServiceType::findOrFail($id);
        $serviceType->update($request->input());
        return redirect('admin/service-type');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ServiceType::destroy($id);
        return redirect()->back();
    }
}
