<?php

namespace App\Http\Controllers\Admin;

use App\ServiceBooking;
use Carbon\Carbon;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ServiceBookingController extends Controller
{

    protected $limit = 20;
    protected $status = [1 => 'Pending', 2 => 'Confirmed', 3 => 'Completed', 4 => 'Canceled'];
    protected $bookingTime = [1 => 'Next Hour', 2 => 'After 2 Hour', 3 => 'After 3 hour', 4 => 'Any Time'];

    /**
     * @return mixed
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param mixed $limit
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    /**
     * retuns the index page
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $serviceBooking = new ServiceBooking();
        $serviceBookings = $serviceBooking->orderBy('id', 'desc')->paginate(15);
        if ($serviceBooking->get()->last() == null) {
            $request->session()->put('isChange', 0);
        } else {
            $request->session()->put('isChange', $serviceBooking->get()->last()->id);
        }
        return view('admin.serviceBooking.index', compact('serviceBookings'));
    }

    /**
     * returns about the new orders
     * returns the data as json array
     *
     * @param Request $request
     * @return array
     */
    public function getData(Request $request)
    {
        $data = null;
        $serviceBooking = new ServiceBooking();
        $lastId = $serviceBooking->get()->last()->id;
        if ($request->session()->get('isChange') < $lastId) {
            $data = ServiceBooking::skip($request->session()->get('isChange'))->take(($lastId - $request->session()->get('isChange')))->get();
            $request->session()->put('isChange', $lastId);
            return ['data' => $this->transformCollection($data->all())];
        }
        return ['data' => $data];
    }


    public function changeStatus(Request $request)
    {
        $order = ServiceBooking::find($request->order_id);
        $order->status = $request->val;
        $order->save();
        return $order;
    }

    /**
     * transform the data into single array
     *
     * @param $data
     * @return array
     */
    public function transformCollection($data)
    {
        return array_reverse(array_map(function ($data) {
            return [
                'id' => $data->id,
                'name' => $data->name,
                'mobile' => $data->mobile,
                'location' => $data->location->location_name,
                'service_type' => $data->serviceType->service_type_name,
                'status' => $this->status[$data->status],
                'booking_date' => Carbon::createFromFormat('Y-m-d', $data->booking_date)->format('d M, Y'),
                'booking_time' => $this->bookingTime[$data->booking_time],
                'booked_at' => $data->created_at->format('d M,Y - h:i a')
            ];
        }, $data));

    }

    /**
     * Already implemented NOT USED
     * it's handles the api pagination
     *
     * @param Paginator $paginator
     * @param $data
     * @return array
     */
    public function respondWithPagination(Paginator $paginator, $data)
    {
        $data = array_merge($data, [
            'total_count' => $paginator->total(),
            'total_page' => ceil($paginator->total() / $paginator->perPage()),
            'current_page' => $paginator->currentPage(),
            'limit' => $paginator->perPage(),
            'next_page' => $paginator->nextPageUrl(),
            'prev_page' => $paginator->previousPageUrl()
        ]);
        return $data;
    }
}
