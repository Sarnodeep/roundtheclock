<?php

namespace App\Http\Controllers\Admin;

use App\Location;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locations = Location::get();
        return view('admin.locations.index', compact('locations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Requests\LocationRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\LocationRequest $request)
    {
        Location::create($request->input());
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $locations = Location::FindOrFail($id);
        return view('admin.locations.edit', compact('locations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Requests\LocationRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\LocationRequest $request, $id)
    {
        $locations = Location::FindOrFail($id);
        $locations->update($request->input());
        return redirect('admin/locations');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Location::destroy($id);
        return redirect()->back();
    }
}
