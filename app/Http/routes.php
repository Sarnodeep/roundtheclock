<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'ServiceBookingController@index');
Route::get('contact', 'ServiceBookingController@contact');
Route::get('about', 'ServiceBookingController@about');
Route::get('service', 'ServiceBookingController@service');
Route::get('privacy', 'ServiceBookingController@Privacy');


Route::auth();

Route::get('register', function () {
    return redirect('/login');
});


/**
 * CMS routes
 */
Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
    Route::get('/', 'Admin\AdminController@index');
    Route::resource('locations', 'Admin\LocationController', ['only' => ['index', 'store', 'edit', 'update', 'destroy']]);
    Route::resource('service-type', 'Admin\ServiceTypeController', ['only' => ['index', 'store', 'edit', 'update', 'destroy']]);
    Route::get('orders', 'Admin\ServiceBookingController@index');
    Route::put('orders', 'Admin\ServiceBookingController@changeStatus');
    Route::get('orders-getData', 'Admin\ServiceBookingController@getData');
    Route::get('users', 'Admin\ServiceBookingController@index');
});

Route::group(['prefix' => 'websiteImage'], function () {
    Route::get('/{fileName}', 'ImageController@index');
});

/**
 * 1.   service-type returns the data
 * 2.   service-book place a booking and returns booking information
 */
Route::group(['prefix' => 'api',], function () {
    Route::get('service-type', 'ServiceBookingController@getServiceType');
    Route::get('service-book', function () {
        abort(404);
    });
    Route::post('service-book', 'ServiceBookingController@bookService');
});