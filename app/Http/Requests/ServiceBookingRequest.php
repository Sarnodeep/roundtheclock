<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ServiceBookingRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'mobile' => 'required|numeric|digits:10',
            'booking_date' => 'required|date',
            'booking_time' => 'required|numeric',
            'service_type_id' => 'required|numeric',
            'location_id' => 'required|numeric'
        ];
    }
}
