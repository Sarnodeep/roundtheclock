<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Location extends Model
{
    use SoftDeletes;
    protected $fillable = ['location_name'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
}
