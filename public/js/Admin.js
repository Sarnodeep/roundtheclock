/**
 * Created by Jhinuk on 5/22/2016.
 */
(function () {
    /**
     * Fetch data from server
     * has a callback function takes response object as parameter
     *
     * @param url
     * @param type
     * @param data
     * @param callBack
     */
    function getData(url, type, data, callBack) {
        $.ajax({
            url: url,
            type: type,
            data: data,
            success: function (response) {
                return callBack(response);
            }
        });
    }

    /**
     * this module controls the entire order retrieving operation
     * it's a type of real time notification system
     *
     * @returns {{init: init}}
     */
    var serviceBookingModule = function () {
        /**
         * store the data
         * @type {Array}
         */
        var serviceBooking = [];

        /**
         * caching the dom
         * @type {*|jQuery|HTMLElement}
         */
        var $el = $('#serviceBookingModule'),
            $tbody = $el.find('tbody'),
            $save_button = $tbody.find('tr>td:last-child>button'),
            template = $tbody.find('#service-booking-template').html();

        var csrf = $el.data('csrf');

        /**
         * _getData From server using the getData method
         * @private
         */
        function _getData() {
            getData('http://localhost/roundTheClock/public/admin/orders-getData', 'GET', {}, function (response) {
                _setData(response);
            });
        }

        /**
         * set data to the variable
         * @param getData
         * @private
         */
        function _setData(getData) {
            $.merge(serviceBooking, getData);
            _render(getData);
        }

        function _changeStatus() {
            $save_button.on('click', function (e) {
                var val = (e.currentTarget).closest('tr');
                $currentItem = $(val).find('[name="status"]');
                //console.log(val);
                getData('orders', 'POST', {
                    _token: csrf,
                    _method: 'PUT',
                    order_id: $currentItem.data('id'),
                    val: $currentItem.val()
                }, function (response) {
                    console.log(response);
                });
            });
        }

        /**
         * renders data to the view
         * @param data
         * @private
         */
        function _render(data) {
            $tbody.find('tr').eq(0).before(Mustache.render(template, {data: data.data}));
        }

        /**
         * its initialize the whole module
         */
        function init() {
            _changeStatus();
            setInterval(function () {
                _getData();
            }, 10000);
        }

        return {
            init: init
        }
    };
    serviceBookingModule().init()
    //ohSnap('Yeeaahh! You are now registered.', {'duration':'2000'});  // 2 seconds
})();
