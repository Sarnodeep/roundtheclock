/**
 * Created by Jhinuk on 5/19/2016.
 */

/**
 * Created by Sarnodeep Agarwalla on 5/24/2015.
 */
var validationModule = function () {

    var $currentForm;

    var $form = $('.validation'),
        $button = $form.find('.validate-button');

    /**
     * triggers the checking
     * @private
     */
    function validate() {
        $button.on('click', _performCheck);
    }

    /**
     * performs the check
     *
     * @param e
     * @returns {boolean}
     * @private
     */
    function _performCheck(e) {
        $currentForm = $(e.currentTarget).parents('.validation');
        _validateRequired();
        return false;
    }

    /**
     *
     * @returns {boolean}
     * @private
     */
    function _validateRequired() {
        var flag = true;
        $currentForm.find('.custom-error-msg').remove();

        $currentForm.find('.validate,.validate-email,.validate-alphaNumeric,.validate-alpha,.validate-numeric').each(function (index, element) {

            var $targetElement = $(element);
            var validateRule;
            $targetElement.parents('.form-group').removeClass('has-error');
            $targetElement.parents('.form-group').removeClass('has-success');

            if ($targetElement.val() != '' && $targetElement.val() != null) {

                /**
                 * only for the required field
                 */
                if ($targetElement.hasClass('validate')) {
                    _addSuccessMessage($targetElement);
                }

                /**
                 * email address validation
                 */
                if ($targetElement.hasClass('validate-email')) {
                    validateRule = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/;
                    if (!checkValidation(validateRule, $targetElement.val())) {
                        var msg = ($targetElement.data('msg')) ? $targetElement.data('msg') : 'Provide a valid email address';
                        _addErrorMessage($targetElement, msg);
                        flag = false;
                    } else
                        _addSuccessMessage($targetElement);

                }

                /**
                 * only alpha field validation
                 */
                if ($targetElement.hasClass('validate-alpha')) {
                    validateRule = /^[a-zA-Z ]*$/;
                    if (!checkValidation(validateRule, $targetElement.val())) {
                        var msg = ($targetElement.data('msg')) ? $targetElement.data('msg') : 'Provide letters only ';
                        _addErrorMessage($targetElement, msg);
                        flag = false;
                    } else
                        _addSuccessMessage($targetElement);
                }

                /**
                 * validate only numeric field
                 */
                if ($targetElement.hasClass('validate-numeric')) {
                    validateRule = /^[0-9]+$/;
                    if (!checkValidation(validateRule, $targetElement.val())) {
                        var msg = ($targetElement.data('msg')) ? $targetElement.data('msg') : 'Provide a valid number';
                        _addErrorMessage($targetElement, msg);
                        flag = false;
                    } else
                        _addSuccessMessage($targetElement);
                }

                /**
                 * validate only alpha numeric
                 */
                if ($targetElement.hasClass('validate-alphaNumeric')) {
                    validateRule = /''/;
                    if (!checkValidation(validateRule, $targetElement.val())) {
                        var msg = ($targetElement.data('msg')) ? $targetElement.data('msg') : 'No special character is allowed';
                        _addErrorMessage($targetElement, msg);
                        flag = false;
                    } else
                        _addSuccessMessage($targetElement);
                }


            } else {
                var msg = ($targetElement.data('blank')) ? $targetElement.data('blank') : 'This field should be filled';
                _addErrorMessage($targetElement, msg);
                flag = false;
            }
        });


        return (flag) ? $currentForm.submit() : flag;
    }

    /**
     * Add error msg
     *
     * @param $targetElement
     * @param msg
     * @private
     */
    function _addErrorMessage($targetElement, msg) {
        $targetElement.closest('div').after($('<span></span>', {
            text: msg,
            class: 'custom-error-msg text-danger'
        }));
        $targetElement.parents('.form-group').addClass('has-error');
    }

    /**
     * Add the success msg
     *
     * @param $targetElement
     * @private
     */
    function _addSuccessMessage($targetElement) {
        var msg = ($targetElement.data('scs')) ? $targetElement.data('scs') : null;
        if (msg != null) {
            $targetElement.closest('div').after($('<span></span>', {
                text: msg,
                class: 'custom-error-msg text-success'
            }));
        }
        $targetElement.parents('.form-group').addClass('has-success');
    }

    /**
     * checks the validation
     *
     * @param validateRule
     * @param value
     * @returns {boolean|*|{options, src}|{src}|{files, tasks}}
     */
    function checkValidation(validateRule, value) {
        return validateRule.test(value);
    }

    return {
        init: validate
    }
};
$(function () {
    validationModule().init();
});


(function () {
    var serviceBookingFormController = (function () {
        var $form = $('#serviceBookingForm'),
            $bookingDate = $form.find('[name=booking_date]');
        $bookingDate.datetimepicker({
            format: 'Y-M-D',
            minDate: moment()
        });

    })();
})();
