<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('mobile', 11);
            $table->date('booking_date');
            $table->enum('booking_time', [1, 2, 3, 4])->default(1);
            $table->integer('service_type_id')->unsigned();
            $table->integer('location_id')->unsigned();
            $table->enum('status', [1, 2, 3, 4])->default(1);
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('service_type_id')
                ->references('id')
                ->on('service_types')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('location_id')
                ->references('id')
                ->on('locations')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('service_bookings');
    }
}
