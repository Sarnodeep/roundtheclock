<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class LocationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $i = 0;
        while ($i < 5) {
            \App\Location::create([
                'location_name' => $faker->name
            ]);
            $i++;
        }
    }
}
