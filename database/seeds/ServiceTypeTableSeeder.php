<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ServiceTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $i = 0;
        while ($i < 10) {
            \App\ServiceType::create([
                'service_type_name' => $faker->name,
                'description' => $faker->paragraph($nbSentences = 2, $variableNbSentences = true)
            ]);
            $i++;
        }
    }
}
