<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ServiceBookingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $i = 0;
        while ($i < 50) {
            \App\ServiceBooking::create([
                'name' => $faker->name,
                'mobile' => rand(7, 9) . rand(100000000, 999999999),
                'booking_date' => date('Y-m-d'),
                'booking_time' => rand(1, 4),
                'service_type_id' => rand(1, 10),
                'location_id' => rand(1, 5)
            ]);
            $i++;
        }
    }
}
