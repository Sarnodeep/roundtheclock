<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'name' => 'Sarnodeep',
            'email' => 'example@admin.com',
            'password' => bcrypt('secret')
        ]);
//        $this->call(UsersTableSeeder::class);
//        $this->call(ServiceTypeTableSeeder::class);
//        $this->call(LocationTableSeeder::class);
//        $this->call(ServiceBookingTableSeeder::class);
    }
}
