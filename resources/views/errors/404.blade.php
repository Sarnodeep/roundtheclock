@extends('web.layout.app')
@section('content')
    <div class="container-fluid" id="mainContainer">
        <h1>Page Not Found
            <small> Error 404.</small>
        </h1>
    </div>
@endsection