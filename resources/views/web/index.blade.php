@extends('web.layout.app')
@section('content')
    <div id="mainContainer"  style="background: url('{!! url('websiteImage/HOMESERVICEPOSTER.jpg') !!}') center">
        <div class="col-lg-8 col-md-10 col-sm-12 col-lg-offset-2 col-md-offset-1">
            <form class="form-horizontal well validation" action="{!! url('api/service-book') !!}" method="POST"
                  id="serviceBookingForm">
                {!! csrf_field() !!}
                <input type="hidden" name="_url" value="web">
                <fieldset>
                    <legend>Book your service</legend>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <label for="inputName" class="control-label ">Name</label>

                            <div class="input-group">
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-user"></span>
                            </span>
                                <input type="text" class="form-control validate-alpha" id="inputName"
                                       placeholder="Your Name" name="name"
                                       required>
                            </div>
                        </div>
                    </div>


                    <div class="form-group">

                        <div class="col-lg-12">
                            <label for="inputMobile" class="control-label">Mobile No</label>

                            <div class="input-group">
                             <span class="input-group-addon">
                                <span class="glyphicon glyphicon-phone"></span>
                            </span>
                                <input type="text" class="form-control validate-numeric" id="inputMobile"
                                       placeholder="XXXXXXXXX" required
                                       maxlength="10" name="mobile">
                            </div>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-lg-6">
                            <label for="service_type" class="control-label">Service Type</label>

                            <div class="input-group">
                             <span class="input-group-addon">
                                <span class="glyphicon glyphicon-tag"></span>
                            </span>
                                <select class="form-control validate" id="service_type" name="service_type_id" required>
                                    @foreach($serviceTypes as $serviceType)
                                        <option value="{!! $serviceType->id !!}">{!! $serviceType->service_type_name !!}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <label for="service_type" class="control-label ">Select Location</label>

                            <div class="input-group">
                             <span class="input-group-addon">
                                <span class="glyphicon glyphicon-map-marker"></span>
                            </span>
                                <select class="form-control validate" id="location" name="location_id" required>
                                    @foreach($locations as $location)
                                        <option value="{!! $location->id !!}">{!! $location->location_name !!}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-lg-6">
                            <label for="booking_date" class="control-label">Date of Booking</label>

                            <div class="input-group">
                             <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                                <input type="text" name="booking_date" id="booking_date" class="form-control validate"
                                       placeholder="yy-mm-dd" required>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <label for="booking_time" class="control-label">Time of Booking</label>

                            <div class="input-group time">
                             <span class="input-group-addon">
                                <span class="glyphicon glyphicon-time"></span>
                            </span>
                                <select name="booking_time" id="booking_time" class="form-control validate" required>
                                    <option value="1">Next Hour</option>
                                    <option value="2">Next 2 Hour</option>
                                    <option value="3">Next 3 Hour</option>
                                    <option value="4">Any Time Today</option>
                                </select>
                            </div>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-lg-3 col-lg-offset-9">
                            <button type="submit" class="btn btn-success validate-button">
                                <span class="glyphicon glyphicon-send" aria-hidden="true"></span>
                                Book Now
                            </button>
                        </div>
                    </div>

                </fieldset>
            </form>
        </div>
    </div>
@endsection