 <!-- Fixed navbar -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a href="{!! url('/') !!}">
          <img src="{!! URL::asset('/websiteImage/android-chrome-192x192.png') !!}" alt="Round the clock" class="img-rounded img-responsive" style="height: 50px">
          </a>
          {{--<a class="navbar-brand" href="{!! url('/') !!}">--}}
            {{--Round the Clock--}}
            {{--<img src="{!! url('/icons/android-chrome-192x192.png') !!}" alt="Round the clock" class="img-rounded img-responsive" style="height: 64px"></a>--}}
          {{--</a>--}}
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="{!! url('service') !!}">Services</a></li>
            <li><a href="{!! url('about') !!}">About us</a></li>
            <li><a href="{!! url('contact') !!}">Contact us</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
