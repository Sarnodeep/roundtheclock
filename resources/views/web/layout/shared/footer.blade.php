<div class="container-fluid ">
    <div class="col-lg-8 col-sm-12 col-md-8">
        <div class="col-lg-6 col-sm-6 col-md-6">
            <h3>Other Links</h3>
            <ul>
                {{--<li><a>Terms and Conditions</a></li>--}}
                <li><a href="{!! url('/privacy') !!}">Privacy Policy</a></li>
            </ul>
        </div>
        <div class="col-lg-6 col-sm-6 col-md-6">
            <h3>Need Help?</h3>
            <ul class="list-group">
                <li class="list-group-item"><span class="glyphicon glyphicon-earphone"></span> +91-9097400410</li>
                <li class="list-group-item"><span class="glyphicon glyphicon-earphone"></span> +91-9798215372</li>
                <li class="list-group-item"><span class="glyphicon glyphicon-envelope"></span> mail2roundtheclock@gmail.com</li>
            </ul>
        </div>
    </div>
    <div class="col-lg-4 col-sm-12 col-md-4">
        <form method="put" action="#" id="suggestionForm" class="validation">
            <fieldset>
                <legend>Suggest Us</legend>
                <div class="form-group">
                    <input type="text" class="form-control validate" name="name" placeholder="your name" required>
                </div>
                <div class="form-group">
                    <input type="email" name="email" class="form-control validate-email"
                           placeholder="yourName@example.com" required>
                </div>
                <div class="form-group">
                    <textarea class="form-control validate" name="suggestion " required></textarea>
                </div>
                <div class="form-group">
                    <button class="btn btn-info validate-button" type="submit">Submit</button>
                </div>
            </fieldset>
        </form>
    </div>
</div>
<div class="col-lg-12">
    Copyright reserved {!! date('Y') !!}
</div>