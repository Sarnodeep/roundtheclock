<!doctype html>
<html>
<head>
    <title>Round the Clock</title>
    <meta charset="utf-8">

    <link rel="apple-touch-icon" sizes="180x180" href="{!! url('websiteImage/apple-touch-icon.png') !!}">
    <link rel="icon" type="image/png" href="{!! url('/websiteImage/favicon-32x32.png') !!}" sizes="32x32">
    <link rel="icon" type="image/png" href="{!! url('/websiteImage/favicon-16x16.png') !!}" sizes="16x16">
    <meta name="theme-color" content="#ffffff">

    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{!! URL::asset('/css/vendor.css') !!}">
    <style>
        html, body {
            height: 100%;
        }

        #mainContainer {
            min-height: 100%;
            padding-top: 70px;
        }

        footer {
            padding-top: 20px;
            background-color: #e3e3e3;
        }

        footer .col-lg-12:last-child {
            background-color: #000011;
            color: rgba(255, 255, 255, 0.6);
        }

        textarea {
            resize: none;
        }

        #contactPage {
            background: url('{!! url('/websiteImage/map.jpg') !!} ') no-repeat center;
            height: 100%;
        }
    </style>
</head>
<body>
@include('web.layout.shared.navbar')


@yield('content')

<footer>
    @include('web.layout.shared.footer')
</footer>
<script src="{!! URL::asset('/js/vendor.js') !!}"></script>
<script src="{!! URl::asset('/js/Script.js') !!}"></script>
</body>
</html>
