@extends('web.layout.app')
@section('content')
    <div class="container-fluid" id="mainContainer">
        <h1>About Us</h1>
        <hr>
        <div class="col-lg-4">
            <img src="{!! url('websiteImage/about.jpg') !!}" alt="Service Image" class="img-responsive">
        </div>
        <section class="col-lg-8">
            <ul style="list-style-type: none">
                <li>
                    <div>
                        <blockquote>
                            <h2>Online Service Provider</h2>
                        </blockquote>
                        <article>
                            Our company is an online service provider all set to mark its presence in this highly
                            managed and
                            synchronized
                            world. The main motive of existence of our company is to provide easy management, repair and
                            maintenance of
                            electronic appliances that too quickly. Not only we promise best service but also quick
                            reliable
                            service. A
                            prosperous company is one who emphasizes more on satisfying the existing customers rather
                            than just
                            increasing
                            the no of customers of company. We believe in customers who are content by our service. We
                            provide
                            24/7 service
                            to our customers.
                        </article>
                        <hr>
                    </div>
                </li>
                <li>
                    <div>
                        <blockquote>
                            <h2>Professionals Experts</h2>
                        </blockquote>
                        <article>
                            We have professionals working experts to solve your problems and deliver high quality and
                            finished
                            results. Our
                            team members are well trained and certified in their field to boost the chances of easy
                            diagnosis
                            and solving of
                            problem quickly. Our professional are assigned to their respective work and expected to give
                            high
                            end solutions
                            to the diagnosed problems.
                        </article>
                        <hr>
                    </div>
                </li>
                <li>
                    <div>
                        <blockquote><h2>Simplicity of availing our services</h2></blockquote>
                        <article>Our unique selling point is the simplicity of availing our services which differentiate
                            us and
                            helps you to
                            choose us among hundreds of different online service provider. To avail our service all you
                            need to
                            do is book
                            your service online on our service portal by providing your basic details, required service
                            type and
                            the
                            preferred service hours. We will get back to you as soon as possible and try to get your
                            problem
                            solved. Yess!!
                            It's as easy as that.
                        </article>
                    </div>
                </li>
            </ul>
        </section>

        <h1>Our Team</h1>
        <hr>
        <div class="container">
            <div class="col-lg-4 col-sm-4 col-md-4" style="overflow: hidden">
                <img src="{!! url('websiteImage/Tarique.jpg') !!}" alt="Tarique" class="img-responsive"
                     style="height: 300px;width: 300px">
                <br>

                <h3><i>Tarique Shamsi</i> <small>Co-Founder</small></h3>
            </div>
            <div class="col-lg-4 col-sm-4 col-md-4" style="overflow: hidden">
                <img src="{!! url('websiteImage/monajir_hussain.jpg') !!}" alt="Tarique" class="img-responsive"
                     style="height: 300px;width: 300px">
                <br>

                <h3><i>Monajir Hussain</i> <small>Co-Founder</small></h3>

            </div>
        </div>
    </div>
@endsection