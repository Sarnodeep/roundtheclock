@extends('web.layout.app')
@section('content')
    <div class="row-fluid" id="contactPage">
        <div id="mainContainer" style="background-color: rgba(0,0,0,0.4)">
            <div class="container" style="color: white">
                <h1>Contact Us</h1>
                <hr>
                <div class="col-lg-4" style="background-color: rgba(0,0,0,0.5) ">
                    <h3><span class="glyphicon glyphicon-earphone"></span>+91-9097400410</h3>

                    <h3><span class="glyphicon glyphicon-earphone"></span>+91-9798215372</h3>

                    <img src="{!! url('/websiteImage/RoundTheClockVisitingCard.jpg') !!}" class="img-responsive"
                         style="height:300px;padding-bottom: 20px;">
                </div>
            </div>
        </div>
    </div>
@endsection