@extends('web.layout.app')
@section('content')
    <div id="mainContainer" class="container-fluid">
        <h1>About Our Services</h1>
        <hr>
        <div class="col-lg-12 col-sm-12 col-md-12">

            <div class="row">
                <div class="col-sm-4">
                    <img src="{!! url('websiteImage/service.jpg') !!}" alt="Service Image" class="img-responsive">
                </div>
                <div class="col-sm-8">
                    @foreach($serviceTypes as $serviceType)
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <p class="panel-title">
                                    <span class="glyphicon glyphicon-tags"> </span> &nbsp;
                                    {!! $serviceType->service_type_name !!}
                                </p>
                            </div>
                            <div class="panel-body">
                                {!! $serviceType->description !!}
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>

        </div>
    </div>
@endsection