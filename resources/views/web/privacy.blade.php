@extends('web.layout.app')
@section('content')
    <div class="container-fluid"  id="mainContainer">
        <h1>
            Our Commitment To Privacy
        </h1>
        <blockquote class="text-success">
            <p>
                Your privacy is important to us. To better protect your privacy we provide this notice explaining our
                online
                information practices and the choices you can make about the way your information is collected and used.
            </p>
        </blockquote>
        <div class="alert alert-warning">
            <blockqoute>
                USAGE OF PERSONAL INFORMATION
            </blockqoute>
        </div>
        <article>
            <p style="column-count: 3">
                Information gathering of customer is done for estimating the requirement of customer and providing
                them with service as required.
                Information may also be used to keep records of the serviced customer so as to improve the product and
                service quality of company
                Information is used to make our customer aware about our new products, special schemes or other
                product related information by periodically sending promotional emails about the same.
                Market Research and analysis can be one of the purposes served by customer information gathering. Our
                medium of contact may vary from email, phone, to fax etc. Further changes will be made to website
                accordingly.
                Security: We are obliged to safeguard and secure all the information provided by customers as a
                precaution to unauthorized access and disclosure. We have physical, electronic and managerial
                procedures to do the needful.
                Cookies: Cookies are the small textual files which are placed on individual system after he permits.
                Analysis of web traffic is done by cookies. It also allows web applications to respond to customers as
                an individual and stores all information regarding individual so as to provide service according to
                their requirements ,likes and dislikes. Cookies helps us to delivers best and high quality experience
                to the customers. cookies by no way have access to customers computers and its information
                information.. acceptance or decline of cookie is a individual choice. Declining may prevent an
                individual to take the overall advantage of website.
                Linked websites: To serve the socializing and marketing purpose our website may show links of other
                website which falls in company interest. We are not bound to provide any control over the linked
                websites and cannot be stated responsible for the protection and privacy of your information you
                provide to linked sites. Privacy statements don't govern such linked sites. Care and precaution should
                be taken while considering about terms , conditions and policy statements of such sites..
                Securing your personal information: some precautions can be taken to secure information by restricting
                use of your personal information by following this particular tips1). Unchecked check boxes that
                insist you to save information for further use: when a website insists you to fill a form, search for
                the checked check box which insist you to save the info. Uncheck the option and proceed further. The
                website won't save any information and as per your indication..
                THIRD PARTY: We refrain from selling, distributing or lease your personal information to third parties
                unless and until we have permit from customer or are required by governing law to do so. Promotional
                Information about third parties can be delivered to you according to your wish by using your personal
                information.
                UPDATION OF INFORMATION: If according to your belief any information you provide seems to be
                incomplete or wrong, we request you to get in contact with us through email or phone.
                Company is liable to change its polices time to time ,as per the requirement. Customers are requested
                to be updated with privacy policy page and check the polices to verify whether they are content with
                all of them. This policy will be implemented from 1st june 2016
            </p>
        </article>
    </div>
@endsection