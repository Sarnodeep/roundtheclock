<div class="list-group">
    <a class="list-group-item active disabled">
        Menu
    </a>
    <a href="{!! url('admin/service-type') !!}" class="list-group-item">
        <span class="glyphicon glyphicon-tags"></span>&nbsp;
        Service Type</a>
    <a href="{!! url('admin/locations') !!}" class="list-group-item">
        <span class="glyphicon glyphicon-map-marker"></span>&nbsp;
        Location</a>
    <a href="{!! url('admin/orders') !!}" class="list-group-item">
        <span class="glyphicon glyphicon-tags"></span>&nbsp;
        Orders</a>
    <a href="{!! url('admin/Users') !!}" class="list-group-item">
        <span class="glyphicon glyphicon-user"></span>&nbsp;
        User</a>
</div>