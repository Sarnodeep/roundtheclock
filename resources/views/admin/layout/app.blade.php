<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Round the clock</title>
    <link rel="stylesheet" href="{!! URL::asset('css/vendor.css') !!}">
    <link rel="apple-touch-icon" sizes="180x180" href="{!! url('icons/apple-touch-icon.png') !!}">
    <link rel="icon" type="image/png" href="{!! url('icons/favicon-32x32.png') !!}" sizes="32x32">
    <link rel="icon" type="image/png" href="{!! url('icons//favicon-16x16.png') !!}" sizes="16x16">
    <meta name="theme-color" content="#ffffff">
</head>
<body>
<header>
    @include('admin.layout.shared._navbar')
</header>

<div class="row-fluid">
    <div class="col-lg-2">
        <aside>
            @include('admin.layout.shared._header')
        </aside>
    </div>
    <div class="col-lg-10">
        @yield('content')
    </div>
</div>
<div class="col-lg-12">
    <footer>
        @include('admin.layout.shared._footer')
    </footer>
</div>
<script src="{!! URL::asset('js/vendor.js') !!}"></script>
@stack('Script')
</body>
</html>