@extends('admin.layout.app')
@section('content')
    <div class="container-fluid">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Create New Service Type
                    </h3>
                </div>
                <div class="panel-body">
                    <form action="{!! url('admin/locations/'.$locations->id) !!}" method="post">
                        {!! csrf_field() !!}
                        <input type="hidden" name="_method" value="PUT">

                        <div class="form-group">
                            <label class="label-control">Location Name</label>
                            <input type="text" name="location_name" class="form-control"
                                   value="{!! $locations->location_name !!}">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Add Location</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection