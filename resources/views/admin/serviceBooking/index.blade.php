@extends('admin.layout.app')
@section('content')
    <div class="container-fluid">
        <div class="col-lg-12">
            <table class="table table-striped table-responsive" id="serviceBookingModule" data-csrf="{!! csrf_token() !!}">
                <thead>
                <tr>
                    <td>
                        Sl no
                    </td>
                    <td>
                        Customer Name
                    </td>
                    <td>
                        Mobile No
                    </td>
                    <td>
                        Service Type
                    </td>
                    <td>
                        Location
                    </td>
                    <td>
                        Order Date
                    </td>
                    <td>
                        Order Time
                    </td>
                    <td>
                        Status
                    </td>
                    <td>
                        Booked At
                    </td>
                    <td>
                        Action
                    </td>

                </tr>
                </thead>
                <tbody>
                <script id="service-booking-template" type="text/template">
                    @{{#data}}
                    <tr class="success">
                        <td>@{{id}}</td>
                        <td>@{{name}}</td>
                        <td>@{{mobile}}</td>
                        <td>@{{service_type}}</td>
                        <td>@{{location}}</td>
                        <td>@{{booking_date}}</td>
                        <td>@{{booking_time}}</td>
                        <td>@{{status}}</td>
                        <td>@{{ booked_at }}</td>
                        <td>
                            {{--<button class="btn btn-xs btn-info" type="button">Save</button>--}}
                        </td>
                    </tr>
                    @{{/data}}
                </script>
                @foreach($serviceBookings as $booking)
                    <tr>
                        <td>{!! $booking->id !!}</td>
                        <td>{!! $booking->name !!}</td>
                        <td>{!! $booking->mobile !!}</td>
                        <td>{!! $booking->serviceType->service_type_name !!}</td>
                        <td>{!! $booking->location->location_name !!}</td>
                        <td>{!! \Carbon\Carbon::createFromFormat('Y-m-d', $booking->booking_date)->format('d M, Y') !!}</td>
                        <td>{!! [1=>'Next Hour',2=>'After 2 Hour',3=>'After 3 hour',4=>'Any Time'][$booking->booking_time] !!}</td>
                        <td>
                            <select name="status" class="form-control" data-id="{!! $booking->id !!}">
                                <option value="1" {!! ($booking->status==1)?'selected':'' !!}>Pending</option>
                                <option value="2" {!! ($booking->status==2)?'selected':'' !!}>Confirmed</option>
                                <option value="3" {!! ($booking->status==3)?'selected':'' !!}>Completed</option>
                                <option value="4" {!! ($booking->status==4)?'selected':'' !!}>Cancelled</option>
                            </select>
                        </td>
                        <td>{!! $booking->created_at->format('d M,Y - h:i a') !!}</td>
                        <td>
                            <button class="btn btn-xs btn-info" type="button">Save</button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        {!! $serviceBookings->render() !!}
    </div>

@endsection
@push('Script')
<script src="{!! URL::asset('js/Admin.js') !!}"></script>
@endpush
