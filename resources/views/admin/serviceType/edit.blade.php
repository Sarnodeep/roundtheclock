@extends('admin.layout.app')
@section('content')
    <div class="container-fluid">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Edit
                    </h3>
                </div>
                <div class="panel-body">
                    <form action="{!! url('admin/service-type/'.$serviceType->id) !!}" method="post">
                        {!! csrf_field() !!}
                        <input type="hidden" name="_method" value="PUT">

                        <div class="form-group">
                            <label class="label-control">Service Name</label>
                            <input type="text" name="service_type_name" class="form-control" value="{!! $serviceType->service_type_name !!}">
                        </div>
                        <div class="form-group">
                            <label class="label-control">Description</label>
                            <textarea type="text" name="description" class="form-control">{!! $serviceType->description !!}
                            </textarea>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection