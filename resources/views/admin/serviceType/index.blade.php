@extends('admin.layout.app')
@section('content')
    <div class="container-fluid">
        <div class="col-lg-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Service Type</h3>
                </div>
                <div class="panel-body">
                    <table class="table table-striped table-responsive">
                        <thead>
                        <tr>
                            <td>Sl no</td>
                            <td>Service Name</td>
                            <td>Description</td>
                            <td>Options</td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($serviceTypes as $serviceType)
                            <tr>
                                <td>{!! $serviceType->id !!}</td>
                                <td>{!! $serviceType->service_type_name !!}</td>
                                <td>{!! $serviceType->description !!}</td>
                                <td>
                                    <form action="{!! url('admin/service-type/'.$serviceType->id) !!}" method="post">
                                        {!! csrf_field() !!}
                                        <input type="hidden" name="_method" value="DELETE">
                                        <a href="{!! url('admin/service-type/'.$serviceType->id.'/edit') !!}"><span
                                                    class="glyphicon glyphicon-pencil"></span></a>
                                        <button type="submit"
                                                style="border: none; padding: 0px;background: transparent"><span
                                                    class="glyphicon glyphicon-trash"></span>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Create New Service Type
                    </h3>
                </div>
                <div class="panel-body">
                    <form action="{!! url('admin/service-type') !!}" method="post">
                        {!! csrf_field() !!}
                        <div class="form-group">
                            <label class="label-control">Service Name</label>
                            <input type="text" name="service_type_name" class="form-control">
                        </div>
                        <div class="form-group">
                            <label class="label-control">Description</label>
                            <textarea type="text" name="description" class="form-control">
                            </textarea>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Add New Service</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection